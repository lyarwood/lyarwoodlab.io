---
title: "whereis lyarwood? - Quick Update #1"
date: 2023-11-08T08:00:00Z
tags: 
  - personal
  - whereis
---

> (15/12/23) I've decided to post the text of a [series](/tags/whereis/) of Instagram posts
> I've made to my private account detailing what I've been up
> to over the last 8 weeks while in hospital.
>
> The original posts and associated pictures can be found
> below, assuming I know you feel free to
> send a follow request!
>
> https://www.instagram.com/lyarwood_/

I'm still waiting on various surgical teams coming to an agreement on my procedure. Essentially 2 stand alone ops interwoven into 1. Antibiotics flowing still but vastly reduced from previous weeks. I also finally have a PICC line so the daily cannula and bloods torture is over for now and my arms can heal. Still hopeful for progress this week, then onto recovery and eventually home.