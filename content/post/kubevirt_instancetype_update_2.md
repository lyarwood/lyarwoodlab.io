---
title: "KubeVirt instancetype.kubevirt.io Update #2"
date: 2022-07-21T15:00:00Z
tags: ['dev', 'kubevirt', 'instancetype', 'instancetypes', 'flavors', 'preferences']
---

{{< figure src="/img/KubeVirt_logo.png" >}}

Welcome to part #2 of this [series](/tags/instancetype/) following the development of instancetypes and preferences within KubeVirt! Please note this is just a development journal of sorts, more formal documentation introducing and describing instancetypes will be forthcoming in the near future!

# What's new

## `s/VirtualMachineFlavor/VirtualMachineInstancetype/g`

https://github.com/kubevirt/kubevirt/pull/8039

If you haven't already guessed from the title of this post the long awaited rename has landed, goodbye flavors and hello instancetypes!

The PR is unfortunately huge but did include an [interesting upgrade problem](https://github.com/kubevirt/kubevirt/pull/8039#issuecomment-1176169224) the [fix](https://github.com/lyarwood/kubevirt/blob/4eb29b83bc52bfd25bd46922af8a8269f2b21913/pkg/virt-operator/resource/generate/rbac/operator.go#L315-L329) for which will require additional cleanup in a future release.

## Versioning with `ControllerRevisions`

https://github.com/kubevirt/kubevirt/pull/7875

Versioning through `ControllerRevisions` has been introduced. As previously discussed the underlying `VirtualMachineInstancetypeSpec` or `VirtualMachinePreferenceSpec` are stored by the `VirtualMachine` controller in a `ControllerRevision` unique to the `VirtualMachine` being started. A reference to the `ControllerRevision` is then added to the `VirtualMachine` for future look ups with the `VirtualMachine` also itself referenced as an owner of these `ControllerRevisions` ensuring their removal when the `VirtualMachine` is deleted.

## Bug fixes

* [Raise conflict if `ResourceRequests` are present in `VirtualMachine`](https://github.com/kubevirt/kubevirt/pull/7981)
* [Apply preferences to any added default network interfaces or missing volume disks](https://github.com/kubevirt/kubevirt/pull/7919)

# Upcoming

## `VirtualMachine` subresource APIs to expand `VirtualMachineInstancetype`

https://github.com/kubevirt/kubevirt/pull/7549

This PR will introduce new `VirtualMachine` subresource APIs to expand a referenced instancetype or set of preferences for an existing `VirtualMachine` or one provided by the caller.

Hopefully these APIs will be useful to users and fellow KubeVirt/OpenShift devs who want to validate or just present a fully rendered version of their `VirtualMachine` in some way.

It's worth noting that during the development of this feature we encountered some interesting [OpenAPI](https://github.com/kubevirt/kubevirt/pull/7549#discussion_r923079616) behaviour that took a while to debug and fix.

## `AutoattachInputDevice` and `PreferredAutoattachInputDevice`

https://github.com/kubevirt/kubevirt/pull/8006

While [working on a possible future migration](https://github.com/kubevirt/common-templates/pull/448) of the [`common-templates`](https://github.com/kubevirt/common-templates) project to using `VirtualMachineInstancetypes` and `VirtualMachinePreferences` it was noted that we had no way of automatically attaching an input device to a `VirtualMachine`.

This change introduces both a `AutoattachInputDevice` attribute to control this in a vanilla `VirtualMachines` and a `PreferredAutoattachInputDevice` preference to control this behaviour from within a set of preferences.

The PR includes a simple rework of the application of [`DevicePreferences`](http://kubevirt.io/api-reference/main/definitions.html#_v1alpha1_devicepreferences), applying them *before* any of the [`Autoattach` logic fires within the `VirtualMachine` controller](https://github.com/kubevirt/kubevirt/blob/5e8b0f9f3f0ded79b7ebdf327fea2d61b9f7659f/pkg/virt-controller/watch/vm.go#L1004-L1009). This allows the `PreferredAutoattach` preferences to control the `Autoattach` logic with the original application of preferences *after* this logic has fired ensuring any remaining preferences are also applied to any new devices.

## `VirtualMachineInstancePreset` deprecation in favor of `VirtualMachineInstancetype`

https://github.com/kubevirt/kubevirt/pull/8069

This proposal still has to be raised formally with the community but as set out in the PR I'd like to start the deprecation cycle of `VirtualMachineInstancePreset` now as `VirtualMachineInstancetype` starts to mature as a replacement.

## Making `DomainSpec` optional

https://github.com/kubevirt/kubevirt/pull/7969

Previous work has gone into removing the need to define [`Disks`](https://github.com/kubevirt/kubevirt/pull/7618) for all referenced [`Volumes`](http://kubevirt.io/api-reference/main/definitions.html#_v1_volume) within a [`VirtualMachineInstanceSpec`](http://kubevirt.io/api-reference/main/definitions.html#_v1_virtualmachineinstancespec) and also ensuring [preferences are applied correctly to the automatically added `Disks`](https://github.com/kubevirt/kubevirt/pull/7919).

The end goal for this work has been to make the entire [`DomainSpec`](http://kubevirt.io/api-reference/main/definitions.html#_v1_domainspec) within [`VirtualMachineInstanceSpec`](http://kubevirt.io/api-reference/main/definitions.html#_v1_virtualmachineinstancespec) optional, hopefully simplifying our `VirtualMachine` definitions further when used in conjunction with instancetypes and preferences.

## Revisit the removal of `SharedInformers`

https://github.com/kubevirt/kubevirt/pull/7935

The use of `SharedInformers` within the webhooks and `VirtualMachine` controller had proven problematic and their use was previously removed.

While no discernable performance impact has been seen thus far this change will likely be revisited again in the near future as many controllers appear to have a pattern [of retrying failed lookups using `SharedInformers` with vanilla client calls](https://github.com/kubernetes/kubernetes/blob/09e8339ae4e575ec40e8ed0fc2829fcb678c0f19/pkg/controller/volume/pvcprotection/pvc_protection_controller.go#L211-L227).


## Referenced `VirtualMachineInstancetypes` can be deleted before a `VirtualMachine` starts

https://github.com/kubevirt/kubevirt/issues/8142

The bug above covers a race with the current versioning implementation. This race allows a user to delete a referenced instancetype before the `VirtualMachine` referencing it has started, stashing a copy of the instancetype in a `ControllerRevision`. For example:

```bash
# ./cluster-up/kubectl.sh apply -f examples/csmall.yaml 
virtualmachineinstancetype.instancetype.kubevirt.io/csmall created
# ./cluster-up/kubectl.sh apply -f examples/vm-cirros-csmall.yaml 
virtualmachine.kubevirt.io/vm-cirros-csmall created
# ./cluster-up/kubectl.sh delete virtualmachineinstancetype/csmall
virtualmachineinstancetype.instancetype.kubevirt.io "csmall" deleted
# ./cluster-up/virtctl.sh start vm-cirros-csmall
Error starting VirtualMachine Internal error occurred: admission webhook "virtualmachine-validator.kubevirt.io" denied the request: Failure to find instancetype: virtualmachineinstancetypes.instancetype.kubevirt.io "csmall" not found
```

I believe we need one or more [finalizers](https://kubernetes.io/blog/2021/05/14/using-finalizers-to-control-deletion/) here ensuring that referenced instancetypes and preferences are not removed before they are stashed in a `ControllerRevision`.

An alternative to this would be to create `ControllerRevisions` within the `VirtualMachine` admission webhooks earlier in the lifecycle of a `VirtualMachine`. I had tried this originally but failed to successfully `Patch` the `VirtualMachine` with a reference back to the `ControllerRevision`, often seeing failures with the `VirtualMachine` controller attempting to reconcile the changes.

## Moving the API to `v1beta1`

With the rename now complete and the future direction hopefully set out above I believe now is a good time to start looking into the graduation of the API itself from the experimental `v1alpha1` stage to something more stable.

The [Kubernetes API versioning documentation](https://kubernetes.io/docs/reference/using-api/#api-versioning) provides the following 
summary of the `beta` version:

> * The software is well tested. Enabling a feature is considered safe. Features are enabled by default.
>
> * The support for a feature will not be dropped, though the details may change.
> 
> * The schema and/or semantics of objects may change in incompatible ways in a subsequent beta or stable release. When this happens, migration instructions are provided. Schema changes may require deleting, editing, and re-creating API objects. The editing process may not be straightforward. The migration may require downtime for applications that rely on the feature.
> 
> * The software is not recommended for production uses. Subsequent releases may introduce incompatible changes. If you have multiple clusters which can be upgraded independently, you may be able to relax this restriction.

I believe the instancetype API can meet these criteria in the near future if it isn't already and so I will be looking to start the process soon.

## User-guide documentation

With the rename complete I have finally started drafting some upstream user-guide documentation that I hope to post in a PR soon.

## Introductory kubevirt.io blog post(s)

Following on from the user-guide documentation I also plan on writing and publishing some material introducing instancetypes and preferences on the [kubevirt.io blog](https://kubevirt.io/blogs/).