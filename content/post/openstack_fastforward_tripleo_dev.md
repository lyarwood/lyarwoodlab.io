---
title: "Openstack TripleO FFU Getting started"
date: 2017-10-24T10:05:47+01:00
tags:  ["openstack", "tripleo", "skiplevelupgrades", "fastforwardupgrades"]
---

{{< figure src="/img/tripleo.jpg" >}}


This post will be a living document where I will detail how
[TripleO](https://tripleo.org) developers can initially provision and iterate
quickly while working on service upgrade tasks the new [fast-forward
upgrade](https://specs.openstack.org/openstack/tripleo-specs/specs/queens/fast-forward-upgrades.html)
feature for in TripleO
[Queens](https://releases.openstack.org/queens/schedule.html).

## Initial environment

This section details how to configure the initial environment with specific
undercloud (UC) and overcloud (OC) versions and layouts using
[tripleo-quickstart](https://docs.openstack.org/tripleo-quickstart/latest/).

### Newton UC & OC

This basic combnination is required for end to end testing of fast forward upgrades:

    $ bash quickstart.sh -R newton $VIRTHOST

Note however that the following changes are required so that vbmc is used by
the undercloud instead of pxe_ssh (removed in Pike): 

https://review.openstack.org/#/q/topic:allow_vbmc_newton

### Master UC & Newton OC

This combination is a useful starting point for developers looking to work on
[tripleo-heat-templates](github.com/openstack/tripleo-heat-templates) changes
for a given service:
    
    $ bash quickstart.sh -R master-undercloud-newton-overcloud $VIRTHOST

The `master-undercloud-newton-overcloud` release config is introduced by the
following change:

https://review.openstack.org/#/c/511464/

### Master UC & Newton OC with only Keystone deployed

https://review.openstack.org/#/q/topic:keystone_only_overcloud

    $ bash quickstart.sh -w $WD -t all -R master-undercloud-newton-overcloud  \
       -c config/general_config/keystone-only.yml \
       -N config/nodes/1ctlr.yml $VIRTHOST

