---
title: "whereis lyarwood? - Beard recovery starts now - Update #4"
date: 2023-11-23T08:00:00Z
tags: 
  - personal
  - whereis
---

Another quick update, just stepped outside for the first time in 3 weeks with Katie that was glorious. STILL waiting to hear what the Endocarditis teams plan is to get me home but the recovery ward are happy with my progress so hopefully it's just a matter of time now.

A few folks continue to go above and beyond so thank you again, we are forever in your debt!