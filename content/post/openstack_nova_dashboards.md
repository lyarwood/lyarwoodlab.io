---
title: "Openstack Nova Victoria Cycle Gerrit Dashboards"
date: 2020-06-28T00:51:01+01:00
tags:  ["openstack", "nova", "gerrit", "gerrit-dash-creator"]
---

{{< figure src="/img/nova.png" >}}

As in previous cycles I've updated some of the Nova specific dashboards
available within the excellent
[gerrit-dash-creator](https://opendev.org/x/gerrit-dash-creator) project and
started using them prior to dropping offline on paternity.

{{< figure src="/img/gerrit.png" >}}

I'd really like to see more use of these dashboards within Nova to help focus
our limited review bandwidth on active and mergeable changes so if you do have
any ideas please fire off reviews and add me in!

For now I've linked to some of the dashboards I've been using most often below
with a brief summary and dump of the current ``.dash`` logic used by the
gerrit-dash-creator tooling to build the Gerrit dashboard URLs.

## [nova-specs](https://review.opendev.org/#/dashboard/?title=Nova+Specs+%252D+Victoria&foreach=project%253Aopenstack%252Fnova%252Dspecs+status%253Aopen+NOT+label%253AWorkflow%253C%253D%252D1+branch%253Amaster+NOT+owner%253Aself&You+are+a+reviewer%252C+but+haven%2527t+voted+in+the+current+revision=file%253A%255Especs%252Fvictoria%252F.%252A+NOT+label%253ACode%252DReview%253C%253D%252D1%252Cself+NOT+label%253ACode%252DReview%253E%253D1%252Cself+reviewer%253Aself+label%253AVerified%253E%253D1%252Czuul&Not+blocked+by+%252D2s=file%253A%255Especs%252Fvictoria%252F.%252A+NOT+label%253ACode%252DReview%253C%253D%252D2+NOT+label%253ACode%252DReview%253E%253D2+NOT+label%253ACode%252DReview%253C%253D%252D1%252Cself+NOT+label%253ACode%252DReview%253E%253D1%252Cself+label%253AVerified%253E%253D1%252Czuul&No+votes+and+spec+is+%253E+1+week+old=file%253A%255Especs%252Fvictoria%252F.%252A+NOT+label%253ACode%252DReview%253E%253D%252D2+age%253A7d+label%253AVerified%253E%253D1%252Czuul&Needs+final+%252B2=file%253A%255Especs%252Fvictoria%252F.%252A+label%253ACode%252DReview%253E%253D2+NOT+label%253ACode%252DReview%253C%253D%252D1%252Cself+NOT+label%253ACode%252DReview%253E%253D1%252Cself+label%253AVerified%253E%253D1%252Czuul++NOT+label%253Aworkflow%253E%253D1&Broken+Specs+%2528doesn%2527t+pass+Zuul%2529=file%253A%255Especs%252Fvictoria%252F.%252A+label%253AVerified%253C%253D%252D1%252Czuul&Dead+Specs+%2528blocked+by+a+%252D2%2529=file%253A%255Especs%252Fvictoria%252F.%252A+label%253ACode%252DReview%253C%253D%252D2&Dead+Specs+%2528Not+Proposed+for+Victoria%2529=NOT+file%253A%255Especs%252Fvictoria%252F.%252A+file%253A%255Especs%252F.%252A&Not+Specs+%2528tox.ini+etc%2529=NOT+file%253A%255Especs%252F.%252A)

The ``openstack/nova-specs`` repo contains [Nova design
specifications](https://specs.openstack.org/openstack/nova-specs/) associated
with both the previous and [current development
release](https://specs.openstack.org/openstack/nova-specs/specs/victoria/index.html).
This dashboard specifically targets the current development release as we
should only see reviews landing in gerrit referring to this release at present.

```
[dashboard]
title = Nova Specs - Victoria
description = Review Inbox
foreach = project:openstack/nova-specs status:open NOT label:Workflow<=-1 branch:master NOT owner:self

[section "You are a reviewer, but haven't voted in the current revision"]
query = file:^specs/victoria/.* NOT label:Code-Review<=-1,self NOT label:Code-Review>=1,self reviewer:self label:Verified>=1,zuul

[section "Not blocked by -2s"]
query = file:^specs/victoria/.* NOT label:Code-Review<=-2 NOT label:Code-Review>=2 NOT label:Code-Review<=-1,self NOT label:Code-Review>=1,self label:Verified>=1,zuul

[section "No votes and spec is > 1 week old"]
query = file:^specs/victoria/.* NOT label:Code-Review>=-2 age:7d label:Verified>=1,zuul

[section "Needs final +2"]
query = file:^specs/victoria/.* label:Code-Review>=2 NOT label:Code-Review<=-1,self NOT label:Code-Review>=1,self label:Verified>=1,zuul  NOT label:workflow>=1

[section "Broken Specs (doesn't pass Zuul)"]
query = file:^specs/victoria/.* label:Verified<=-1,zuul

[section "Dead Specs (blocked by a -2)"]
query = file:^specs/victoria/.* label:Code-Review<=-2

[section "Dead Specs (Not Proposed for Victoria)"]
query = NOT file:^specs/victoria/.* file:^specs/.*

[section "Not Specs (tox.ini etc)"]
query = NOT file:^specs/.*
```

## [nova-libvirt](https://review.opendev.org/#/dashboard/?title=Nova+Libvirt+Driver+Review+Inbox&foreach=project%253Aopenstack%252Fnova%250Astatus%253Aopen%250ANOT+owner%253Aself%250ANOT+label%253AWorkflow%253C%253D%252D1%250Alabel%253AVerified%253E%253D1%252Czuul%250ANOT+reviewedby%253Aself%250Abranch%253Amaster%250A%2528file%253A%255Enova%252Fvirt%252Flibvirt%252F.%252A+OR+file%253A%255Enova%252Ftests%252Funit%252Flibvirt%252F.%252A+OR+file%253A%255Enova%252Ftests%252Ffunctional%252Flibvirt%252F.%252A%2529&Small+patches=NOT+label%253ACode%252DReview%253E%253D2%252Cself+NOT+label%253ACode%252DReview%253C%253D%252D1%252Cnova%252Dcore+NOT+message%253A%2522DNM%2522+delta%253A%253C%253D10&Needs+final+%252B2=NOT+label%253ACode%252DReview%253E%253D2%252Cself+label%253ACode%252DReview%253E%253D2+limit%253A50+NOT+label%253Aworkflow%253E%253D1&Bug+fix%252C+Passed+Zuul%252C+No+Negative+Feedback=NOT+label%253ACode%252DReview%253E%253D2%252Cself+NOT+label%253ACode%252DReview%253C%253D%252D1%252Cnova%252Dcore+message%253A%2522bug%253A+%2522+limit%253A50&Wayward+Changes+%2528Changes+with+no+code+review+in+the+last+two+days%2529=NOT+label%253ACode%252DReview%253C%253D%252D1+NOT+label%253ACode%252DReview%253E%253D1+age%253A2d+limit%253A50&Needs+feedback+%2528Changes+older+than+5+days+that+have+not+been+reviewed+by+anyone%2529=NOT+label%253ACode%252DReview%253C%253D%252D1+NOT+label%253ACode%252DReview%253E%253D1+age%253A5d+limit%253A50&Passed+Zuul%252C+No+Negative+Feedback=NOT+label%253ACode%252DReview%253E%253D2+NOT+label%253ACode%252DReview%253C%253D%252D1+limit%253A50&Needs+revisit+%2528You+were+a+reviewer+but+haven%2527t+voted+in+the+current+revision%2529=reviewer%253Aself+limit%253A50)

I [introduced](https://review.opendev.org/#/c/728413/) this dashboard after the
creation of the [libvirt
subteam](https://etherpad.opendev.org/p/nova-libvirt-subteam) recently during
the U cycle. As you can see from the ``foreach`` filter the dashboard only
lists changes touching the standard set of libvirt driver related files within
the ``openstack/nova`` codebase. IMHO I think a dashboard for non-libvirt
drivers would also be useful.

```
[dashboard]
title = Nova Libvirt Driver Review Inbox
description = Review Inbox for the Nova Libvirt Driver
foreach =  project:openstack/nova 
           status:open
           NOT owner:self
           NOT label:Workflow<=-1
           label:Verified>=1,zuul
           NOT reviewedby:self
           branch:master
           (file:^nova/virt/libvirt/.* OR file:^nova/tests/unit/libvirt/.* OR file:^nova/tests/functional/libvirt/.*)

[section "Small patches"]
query = NOT label:Code-Review>=2,self NOT label:Code-Review<=-1,nova-core NOT message:"DNM" delta:<=10

[section "Needs final +2"]
query = NOT label:Code-Review>=2,self label:Code-Review>=2 limit:50 NOT label:workflow>=1

[section "Bug fix, Passed Zuul, No Negative Feedback"]
query = NOT label:Code-Review>=2,self NOT label:Code-Review<=-1,nova-core message:"bug: " limit:50

[section "Wayward Changes (Changes with no code review in the last two days)"]
query = NOT label:Code-Review<=-1 NOT label:Code-Review>=1 age:2d limit:50

[section "Needs feedback (Changes older than 5 days that have not been reviewed by anyone)"]
query = NOT label:Code-Review<=-1 NOT label:Code-Review>=1 age:5d limit:50

[section "Passed Zuul, No Negative Feedback"]
query = NOT label:Code-Review>=2 NOT label:Code-Review<=-1 limit:50

[section "Needs revisit (You were a reviewer but haven't voted in the current revision)"]
query = reviewer:self limit:50
```

## [nova-stable](https://review.opendev.org/#/dashboard/?title=Nova+Stable+Maintenance+Review+Inbox&foreach=%2528project%253Aopenstack%252Fnova+OR+project%253Aopenstack%252Fpython%252Dnovaclient%2529+status%253Aopen+NOT+owner%253Aself+NOT+label%253AWorkflow%253C%253D%252D1+label%253AVerified%253E%253D1%252Czuul+NOT+reviewedby%253Aself&+stable%252Fussuri+You+are+a+reviewer%252C+but+haven%2527t+voted+in+the+current+revision=NOT+label%253ACode%252DReview%253C%253D%252D1%252Cself+NOT+label%253ACode%252DReview%253E%253D1%252Cself+reviewer%253Aself+branch%253Astable%252Fussuri&stable%252Fussuri+Needs+final+%252B2=label%253ACode%252DReview%253E%253D2+NOT%2528reviewerin%253Astable%252Dmaint%252Dcore+label%253ACode%252DReview%253C%253D%252D1%2529+limit%253A50+NOT+label%253Aworkflow%253E%253D1+branch%253Astable%252Fussuri&stable%252Fussuri+Passed+Zuul%252C+No+Negative+Core+Feedback=NOT+label%253ACode%252DReview%253E%253D2+NOT%2528reviewerin%253Astable%252Dmaint%252Dcore+label%253ACode%252DReview%253C%253D%252D1%2529+limit%253A50+branch%253Astable%252Fussuri&+stable%252Ftrain+You+are+a+reviewer%252C+but+haven%2527t+voted+in+the+current+revision=NOT+label%253ACode%252DReview%253C%253D%252D1%252Cself+NOT+label%253ACode%252DReview%253E%253D1%252Cself+reviewer%253Aself+branch%253Astable%252Ftrain&stable%252Ftrain+Needs+final+%252B2=label%253ACode%252DReview%253E%253D2+NOT%2528reviewerin%253Astable%252Dmaint%252Dcore+label%253ACode%252DReview%253C%253D%252D1%2529+limit%253A50+NOT+label%253Aworkflow%253E%253D1+branch%253Astable%252Ftrain&stable%252Ftrain+Passed+Zuul%252C+No+Negative+Core+Feedback=NOT+label%253ACode%252DReview%253E%253D2+NOT%2528reviewerin%253Astable%252Dmaint%252Dcore+label%253ACode%252DReview%253C%253D%252D1%2529+limit%253A50+branch%253Astable%252Ftrain&+stable%252Fstein+You+are+a+reviewer%252C+but+haven%2527t+voted+in+the+current+revision=NOT+label%253ACode%252DReview%253C%253D%252D1%252Cself+NOT+label%253ACode%252DReview%253E%253D1%252Cself+reviewer%253Aself+branch%253Astable%252Fstein&stable%252Fstein+Needs+final+%252B2=label%253ACode%252DReview%253E%253D2+NOT%2528reviewerin%253Astable%252Dmaint%252Dcore+label%253ACode%252DReview%253C%253D%252D1%2529+limit%253A50+NOT+label%253Aworkflow%253E%253D1+branch%253Astable%252Fstein&stable%252Fstein+Passed+Zuul%252C+No+Negative+Core+Feedback=NOT+label%253ACode%252DReview%253E%253D2+NOT%2528reviewerin%253Astable%252Dmaint%252Dcore+label%253ACode%252DReview%253C%253D%252D1%2529+limit%253A50+branch%253Astable%252Fstein)

I have been a Nova Stable Core for a few years now and during the time I have
relied heavily on Gerrit dashboards and queries to help keep track of changes
as they move through our many stable branches. This has been made slightly more
complex by the introduction of
[extended-maintenance](https://docs.openstack.org/project-team-guide/stable-branches.html#extended-maintenance)
branches but more on that below. For now this dashboard covers the ussuri,
train and stein stable branches.

I'm currently using the by branch Nova stable dashboards as these allow me
to track changes through each required branch easily without any additional
clicking within Gerrit. There is however an allinone dashboard if you prefer
that approach.

Finally, for anyone paying attention you might have noticed I'm also using a
[nova-merged](https://review.opendev.org/#/q/status:merged+branch:master+project:openstack/nova)
query in Gerrit to track recently merged changes into master. This has helped
me catch and proactively backport useful fixes to stable many times.

```
[dashboard]
title = Nova Stable Maintenance Review Inbox
description = Review Inbox
foreach = (project:openstack/nova OR project:openstack/python-novaclient) status:open NOT owner:self NOT label:Workflow<=-1 label:Verified>=1,zuul NOT reviewedby:self

[section " stable/ussuri You are a reviewer, but haven't voted in the current revision"]
query = NOT label:Code-Review<=-1,self NOT label:Code-Review>=1,self reviewer:self branch:stable/ussuri

[section "stable/ussuri Needs final +2"]
query = label:Code-Review>=2 NOT(reviewerin:stable-maint-core label:Code-Review<=-1) limit:50 NOT label:workflow>=1 branch:stable/ussuri

[section "stable/ussuri Passed Zuul, No Negative Core Feedback"]
query = NOT label:Code-Review>=2 NOT(reviewerin:stable-maint-core label:Code-Review<=-1) limit:50 branch:stable/ussuri

[section " stable/train You are a reviewer, but haven't voted in the current revision"]
query = NOT label:Code-Review<=-1,self NOT label:Code-Review>=1,self reviewer:self branch:stable/train

[section "stable/train Needs final +2"]
query = label:Code-Review>=2 NOT(reviewerin:stable-maint-core label:Code-Review<=-1) limit:50 NOT label:workflow>=1 branch:stable/train

[section "stable/train Passed Zuul, No Negative Core Feedback"]
query = NOT label:Code-Review>=2 NOT(reviewerin:stable-maint-core label:Code-Review<=-1) limit:50 branch:stable/train

[section " stable/stein You are a reviewer, but haven't voted in the current revision"]
query = NOT label:Code-Review<=-1,self NOT label:Code-Review>=1,self reviewer:self branch:stable/stein

[section "stable/stein Needs final +2"]
query = label:Code-Review>=2 NOT(reviewerin:stable-maint-core label:Code-Review<=-1) limit:50 NOT label:workflow>=1 branch:stable/stein

[section "stable/stein Passed Zuul, No Negative Core Feedback"]
query = NOT label:Code-Review>=2 NOT(reviewerin:stable-maint-core label:Code-Review<=-1) limit:50 branch:stable/stein
```
## [nova-em](https://review.opendev.org/#/dashboard/?title=Nova+Extended+Maintenance+Review+Inbox&foreach=%2528project%253Aopenstack%252Fnova+OR+project%253Aopenstack%252Fpython%252Dnovaclient%2529+status%253Aopen+NOT+owner%253Aself+NOT+label%253AWorkflow%253C%253D%252D1+label%253AVerified%253E%253D1%252Czuul+NOT+reviewedby%253Aself&+stable%252Frocky+You+are+a+reviewer%252C+but+haven%2527t+voted+in+the+current+revision=NOT+label%253ACode%252DReview%253C%253D%252D1%252Cself+NOT+label%253ACode%252DReview%253E%253D1%252Cself+reviewer%253Aself+branch%253Astable%252Frocky&stable%252Frocky+Needs+final+%252B2=label%253ACode%252DReview%253E%253D2+NOT%2528reviewerin%253Astable%252Dmaint%252Dcore+label%253ACode%252DReview%253C%253D%252D1%2529+limit%253A50+NOT+label%253Aworkflow%253E%253D1+branch%253Astable%252Frocky&stable%252Frocky+Passed+Zuul%252C+No+Negative+Core+Feedback=NOT+label%253ACode%252DReview%253E%253D2+NOT%2528reviewerin%253Astable%252Dmaint%252Dcore+label%253ACode%252DReview%253C%253D%252D1%2529+limit%253A50+branch%253Astable%252Frocky&+stable%252Fqueens+You+are+a+reviewer%252C+but+haven%2527t+voted+in+the+current+revision=NOT+label%253ACode%252DReview%253C%253D%252D1%252Cself+NOT+label%253ACode%252DReview%253E%253D1%252Cself+reviewer%253Aself+branch%253Astable%252Fqueens&stable%252Fqueens+Needs+final+%252B2=label%253ACode%252DReview%253E%253D2+NOT%2528reviewerin%253Astable%252Dmaint%252Dcore+label%253ACode%252DReview%253C%253D%252D1%2529+limit%253A50+NOT+label%253Aworkflow%253E%253D1+branch%253Astable%252Fqueens&stable%252Fqueens+Passed+Zuul%252C+No+Negative+Core+Feedback=NOT+label%253ACode%252DReview%253E%253D2+NOT%2528reviewerin%253Astable%252Dmaint%252Dcore+label%253ACode%252DReview%253C%253D%252D1%2529+limit%253A50+branch%253Astable%252Fqueens&+stable%252Fpike+You+are+a+reviewer%252C+but+haven%2527t+voted+in+the+current+revision=NOT+label%253ACode%252DReview%253C%253D%252D1%252Cself+NOT+label%253ACode%252DReview%253E%253D1%252Cself+reviewer%253Aself+branch%253Astable%252Fpike&stable%252Fpike+Needs+final+%252B2=label%253ACode%252DReview%253E%253D2+NOT%2528reviewerin%253Astable%252Dmaint%252Dcore+label%253ACode%252DReview%253C%253D%252D1%2529+limit%253A50+NOT+label%253Aworkflow%253E%253D1+branch%253Astable%252Fpike&stable%252Fpike+Passed+Zuul%252C+No+Negative+Core+Feedback=NOT+label%253ACode%252DReview%253E%253D2+NOT%2528reviewerin%253Astable%252Dmaint%252Dcore+label%253ACode%252DReview%253C%253D%252D1%2529+limit%253A50+branch%253Astable%252Fpike)

In addition to the nova-stable dashboard above I also have a dashboard for our
[extended-maintenance](https://docs.openstack.org/project-team-guide/stable-branches.html#extended-maintenance)
branches. At present these are (or are about to be) rocky, queens and pike.

```
[dashboard]
title = Nova Extended Maintenance Review Inbox
description = Review Inbox
foreach = (project:openstack/nova OR project:openstack/python-novaclient) status:open NOT owner:self NOT label:Workflow<=-1 label:Verified>=1,zuul NOT reviewedby:self

[section " stable/rocky You are a reviewer, but haven't voted in the current revision"]
query = NOT label:Code-Review<=-1,self NOT label:Code-Review>=1,self reviewer:self branch:stable/rocky

[section "stable/rocky Needs final +2"]
query = label:Code-Review>=2 NOT(reviewerin:stable-maint-core label:Code-Review<=-1) limit:50 NOT label:workflow>=1 branch:stable/rocky

[section "stable/rocky Passed Zuul, No Negative Core Feedback"]
query = NOT label:Code-Review>=2 NOT(reviewerin:stable-maint-core label:Code-Review<=-1) limit:50 branch:stable/rocky

[section " stable/queens You are a reviewer, but haven't voted in the current revision"]
query = NOT label:Code-Review<=-1,self NOT label:Code-Review>=1,self reviewer:self branch:stable/queens

[section "stable/queens Needs final +2"]
query = label:Code-Review>=2 NOT(reviewerin:stable-maint-core label:Code-Review<=-1) limit:50 NOT label:workflow>=1 branch:stable/queens

[section "stable/queens Passed Zuul, No Negative Core Feedback"]
query = NOT label:Code-Review>=2 NOT(reviewerin:stable-maint-core label:Code-Review<=-1) limit:50 branch:stable/queens

[section " stable/pike You are a reviewer, but haven't voted in the current revision"]
query = NOT label:Code-Review<=-1,self NOT label:Code-Review>=1,self reviewer:self branch:stable/pike

[section "stable/pike Needs final +2"]
query = label:Code-Review>=2 NOT(reviewerin:stable-maint-core label:Code-Review<=-1) limit:50 NOT label:workflow>=1 branch:stable/pike

[section "stable/pike Passed Zuul, No Negative Core Feedback"]
query = NOT label:Code-Review>=2 NOT(reviewerin:stable-maint-core label:Code-Review<=-1) limit:50 branch:stable/pike
```
