---
title: "DevConf.cz 2024 presentation - Streamlining VM creation within KubeVirt"
date: 2024-06-14T09:00:00Z
tags: 
  - dev
  - kubevirt
  - instancetypes
  - preferences
  - presentation
  - devconf
---

I had the pleasure of delivering the following talk in person this year at devconf in Brno.

There were some challenges with audio and my delivery was rusty at best but the presentation is hopefully understandable and useful for folks.

There were plenty of questions at the end of the talk and even more in the hallway/booth track outside the lecture hall. Feel free to comment on the post if you have more or reach out directly using my contact details on the opening slide.

## Video

{{< youtube id=0TTo1_etW40 start=17297 >}}

## Slides

<iframe width="100%" height="400" name="iframe" src="/presentations/kubevirt/devconf/2024/presentation.html">
</iframe>