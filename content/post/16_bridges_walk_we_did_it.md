+++
title = "16 bridges charity walk - We did it!"
date = "2017-09-19T16:11:58+05:30"
Tags = ["charity", "walking"]
+++

<img src="/img/walk.jpg" width="400" height="400" />

I'm finally back from a work trip to the US and wanted share that we completed
the ~~16 bridges~~ 15 bridges (as the Golden Jubilee Bridge(s) remain closed)
in just over 5 hours 10 days ago!

<iframe src='https://connect.garmin.com/modern/activity/embed/1967615445' title='15 bridges charity walk' width='465' height='500' frameborder='0'></iframe>

Again, our thanks to everyone who donated, it's going to a wonderful charity
and will hopefully make a difference to the lives of people living with
cardiomyopathy!

I've already started looking into similar walks we could take part in next
year, with an obvious candidate of the [Wye Valley
Challenge](http://www.wyevalleychallenge.com/the-challenge/walk-it) being most
likely at the moment. The full 100km version from Chepstow to Hereford might
prove slightly too much for a novice like me however there are shorter, 45km
versions ending in Hereford.
