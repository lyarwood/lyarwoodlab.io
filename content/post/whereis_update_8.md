---
title: "whereis lyarwood? - Fin - Discharged and heading home"
date: 2023-12-21T08:00:00Z
tags: 
  - personal
  - whereis
---

{{< figure src="/img/fin.png" >}}

Fin - Discharged and heading home.

Thanks again to anyone who liked, messaged, visited or helped out back home. Expect my normal spamming of Rose and Phoebe pictures to begin again soon.