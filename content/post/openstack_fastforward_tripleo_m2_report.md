---
title: "OpenStack TripleO FFU M2 progress report"
date: 2017-12-08T18:00:00+01:00
tags:  ["openstack", "tripleo", "skiplevelupgrades", "fastforwardupgrades"]
---

{{< figure src="/img/tripleo.jpg" >}}

http://lists.openstack.org/pipermail/openstack-dev/2017-December/125321.html

This is a brief progress report from the Upgrades squad for the fast-forward
upgrades (FFU) feature in TripleO, introducing N to Q upgrades.

tl;dr Good initial progress, missed M2 goal of nv CI jobs, pushing on to M3.

## Overview

For anyone unfamiliar with the concept of fast-forward upgrades the following
sentence from the spec gives a brief high level introduction:

~~~
> Fast-forward upgrades are upgrades that move an environment from release `N`
> to `N+X` in a single step, where `X` is greater than `1` and for fast-forward
> upgrades is typically `3`.
~~~

The spec itself obviously goes into more detail and I'd recommend anyone
wanting to know more about our approach for FFU in TripleO to start there:

https://specs.openstack.org/openstack/tripleo-specs/specs/queens/fast-forward-upgrades.html

Note that the spec is being updated at present by the following change,
introducing more details on the FFU task layout, ordering, dependency on
the on-going major upgrade rework in Q, canary compute validation etc:

WIP ffu: Spec update for M2
https://review.openstack.org/#/c/526353/

## M2 Status

The original goal for Queens M2 was to have one or more non-voting FFU jobs
deployed *somewhere* able to run through the basic undercloud and overcloud
upgrade workflows, exercising as many compute service dependencies as we could
up to and including Nova. Unfortunately while Sofer has made some great
progress with this we do not have any running FFU jobs at present:

http://lists.openstack.org/pipermail/openstack-dev/2017-December/125316.html

We do however have documented demos that cover FFU for some limited overcloud
environments from Newton to Queens:

OpenStack TripleO FFU Keystone Demo N to Q
https://blog.yarwood.me.uk/2017/11/16/openstack_fastforward_tripleo_keystone/

OpenStack TripleO FFU Nova Demo N to Q
https://blog.yarwood.me.uk/2017/12/01/openstack_fastforward_tripleo_nova/

These demos currently use a stack of changes against THT with the first ~4 or
so changes introducing the FFU framework:

https://review.openstack.org/#/q/status:open+project:openstack/tripleo-heat-templates+branch:master+topic:bp/fast-forward-upgrades

FWIW getting these initial changes merged would help avoid the current change
storm every time this series is rebased to pick up upgrade or deploy related
bug fixes.

Also note that the demos currently use the raw Ansible playbooks stack outputs
to run through the FFU tasks, upgrade tasks and deploy tasks.  This is by no
means what the final UX will be, with python-tripleoclient and workflow work to
be completed ahead of M3.

## M3 Goals

The squad will be focusing on the following goals for M3:

- Non-voting RDO CI jobs defined and running
- FFU THT changes tested by the above jobs and merged
- python-tripleoclient & required Mistral workflows merged
- Use of ceph-ansible for Ceph upgrades
- Draft developer and user docs under review

## FFU squad

Finally, a quick note to highlight that this report marks the end of my own
personal involvement with the FFU feature in TripleO. I'm not going far,
returning to work on Nova and happy to make time to talk about and review FFU
related changes etc. The members of the upgrade squad taking this forward and
your main points of contact for FFU in TripleO will be:

- Sofer (chem)
- Lukas (social)
- Marios (marios)

My thanks again to Sofer, Lukas, Marios, the rest of the upgrade squad and
wider TripleO community for your guidance and patience when putting up with my
constant inane questioning regarding FFU over the past few months!
