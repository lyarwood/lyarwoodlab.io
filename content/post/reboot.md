+++
title = "Hey, it has been a while.."
date = "2017-07-22T16:11:58+05:30"
+++

So it has been a while since I published anything on my original Wordpress
based blog, so long in fact that I've decided to do away with the old and move
onto something new, shiny and statically generated. All previously written and
frankly rather embarrassing content has been deleted and lost forever.

This new blog, for anyone that is interested, is now hosted on [GitLab
pages](https://about.gitlab.com/features/pages/) and statically generated using
[Hugo](https://gohugo.io/). I hopefully have some kind of flare in the mail for
switching away from Wordpress to these new shiny things, I could always use
more flare.

Anyway, moving forward this blog is going to capture some of my work currently
around [OpenStack](http://openstack.org), charity events for [Cardiomyopathy
UK](http://www.cardiomyopathy.org/), hobby projects such as automating my home
with [Home Assistant](https://home-assistant.io/) , ramblings about the
software industry while also documenting my numerous spelling and grammatical
failures forever.

That's enough text for this test post, on with the actual content! 
