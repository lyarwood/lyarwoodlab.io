---
title: "KubeVirt instancetype.kubevirt.io Demo #2"
date: 2022-08-03T12:00:00Z
asciinema: true
tags: 
  - dev
  - kubevirt
  - instancetype
  - instancetypes
  - flavors
  - preferences
  - demo
---

{{< figure src="/img/tslaq-tesla-inc.gif" >}}

In my initial [VirtualMachineFlavor Update #1 post](/2022/06/23/kubevirt_flavors_update_1/) I included an asciinema recorded demo towards the end. I've re-recorded the demo given the recent rename and I've also created a [personal repo of demos](https://github.com/lyarwood/demos/tree/main/kubevirt/instancetypes/2-instancetypes-and-preferences) to store the original script and recordings outside of asciinema.

## Recording

{{< asciinema key="kubevirt/instancetypes/2-instancetypes-and-preferences/demo.cast" preload="0" fit="width">}}

## Transcript 

```yaml
#
# [..] 
#
# Agenda
# 1. The Basics
# 2. VirtualMachineClusterInstancetype and VirtualMachineClusterPreference
# 3. VirtualMachineInstancetype vs VirtualMachinePreference vs VirtualMachine
# 4. Versioning
# 5. What's next... 
#    - https://github.com/kubevirt/kubevirt/issues/7897
#    - https://blog.yarwood.me.uk/2022/07/21/kubevirt_instancetype_update_2/
#

#
# Demo #1 The Basics
#
# Lets start by creating a simple namespaced VirtualMachineInstancetype, VirtualMachinePreference and VirtualMachine
cat <<EOF | ./cluster-up/kubectl.sh apply -f -
---
apiVersion: instancetype.kubevirt.io/v1alpha1
kind: VirtualMachineInstancetype
metadata:
  name: small
spec:
  cpu:
    guest: 2
  memory:
    guest: 128Mi
---
apiVersion: instancetype.kubevirt.io/v1alpha1
kind: VirtualMachinePreference
metadata:
  name: cirros
spec:
  devices:
    preferredDiskBus: virtio
    preferredInterfaceModel: virtio
---
apiVersion: kubevirt.io/v1
kind: VirtualMachine
metadata:
  name: demo
spec:
  instancetype:
    kind: VirtualMachineInstancetype
    name: small
  preference:
    kind: VirtualMachinePreference
    name: cirros
  running: false
  template:
    spec:
      domain:
        devices: {}
      volumes:
      - containerDisk:
          image: registry:5000/kubevirt/cirros-container-disk-demo:devel
        name: containerdisk
      - cloudInitNoCloud:
          userData: |
            #!/bin/sh

            echo 'printed from cloud-init userdata'
        name: cloudinitdisk
EOF
selecting docker as container runtime
virtualmachineinstancetype.instancetype.kubevirt.io/small created
virtualmachinepreference.instancetype.kubevirt.io/cirros created
virtualmachine.kubevirt.io/demo created
# # Starting the VirtualMachine applies the VirtualMachineInstancetype and VirtualMachinePreference to the VirtualMachineInstance
./cluster-up/virtctl.sh start demo &&  ./cluster-up/kubectl.sh wait vms/demo --for=condition=Ready
selecting docker as container runtime
VM demo was scheduled to start
selecting docker as container runtime
virtualmachine.kubevirt.io/demo condition met
# #
# We can check this by comparing the two VirtualMachineInstanceSpec fields from the VirualMachine and VirtualMachineInstance
diff --color -u <( ./cluster-up/kubectl.sh get vms/demo -o json | jq .spec.template.spec) <( ./cluster-up/kubectl.sh get vmis/demo -o json | jq .spec)
selecting docker as container runtime
selecting docker as container runtime
--- /dev/fd/63	2022-08-03 13:36:29.588992874 +0100
+++ /dev/fd/62	2022-08-03 13:36:29.588992874 +0100
@@ -1,15 +1,65 @@
 {
   "domain": {
-    "devices": {},
+    "cpu": {
+      "cores": 1,
+      "model": "host-model",
+      "sockets": 2,
+      "threads": 1
+    },
+    "devices": {
+      "disks": [
+        {
+          "disk": {
+            "bus": "virtio"
+          },
+          "name": "containerdisk"
+        },
+        {
+          "disk": {
+            "bus": "virtio"
+          },
+          "name": "cloudinitdisk"
+        }
+      ],
+      "interfaces": [
+        {
+          "bridge": {},
+          "model": "virtio",
+          "name": "default"
+        }
+      ]
+    },
+    "features": {
+      "acpi": {
+        "enabled": true
+      }
+    },
+    "firmware": {
+      "uuid": "c89d1344-ee03-5c55-99bd-5df16b72bea0"
+    },
     "machine": {
       "type": "q35"
     },
-    "resources": {}
+    "memory": {
+      "guest": "128Mi"
+    },
+    "resources": {
+      "requests": {
+        "memory": "128Mi"
+      }
+    }
   },
+  "networks": [
+    {
+      "name": "default",
+      "pod": {}
+    }
+  ],
   "volumes": [
     {
       "containerDisk": {
-        "image": "registry:5000/kubevirt/cirros-container-disk-demo:devel"
+        "image": "registry:5000/kubevirt/cirros-container-disk-demo:devel",
+        "imagePullPolicy": "IfNotPresent"
       },
       "name": "containerdisk"
     },
# #
# Demo #2 Cluster wide CRDs
# #
# We also have cluster wide instancetypes and preferences we can use, note these are the default if no kind is provided within the VirtualMachine.
cat <<EOF | ./cluster-up/kubectl.sh apply -f -
---
apiVersion: instancetype.kubevirt.io/v1alpha1
kind: VirtualMachineClusterInstancetype
metadata:
  name: small-cluster
spec:
  cpu:
    guest: 2
  memory:
    guest: 128Mi
---
apiVersion: instancetype.kubevirt.io/v1alpha1
kind: VirtualMachineClusterPreference
metadata:
  name: cirros-cluster
spec:
  devices:
    preferredDiskBus: virtio
    preferredInterfaceModel: virtio
---
apiVersion: kubevirt.io/v1
kind: VirtualMachine
metadata:
  name: demo-cluster
spec:
  instancetype:
    name: small-cluster
  preference:
    name: cirros-cluster
  running: false
  template:
    spec:
      domain:
        devices: {}
      volumes:
      - containerDisk:
          image: registry:5000/kubevirt/cirros-container-disk-demo:devel
        name: containerdisk
      - cloudInitNoCloud:
          userData: |
            #!/bin/sh

            echo 'printed from cloud-init userdata'
        name: cloudinitdisk
EOF
selecting docker as container runtime
virtualmachineclusterinstancetype.instancetype.kubevirt.io/small-cluster created
virtualmachineclusterpreference.instancetype.kubevirt.io/cirros-cluster created
virtualmachine.kubevirt.io/demo-cluster created
# #
# InstancetypeMatcher and PreferenceMatcher default to the Cluster CRD Kinds
./cluster-up/kubectl.sh get vms/demo-cluster -o json | jq '.spec.instancetype, .spec.preference'
selecting docker as container runtime
{
  "kind": "virtualmachineclusterinstancetype",
  "name": "small-cluster"
}
{
  "kind": "virtualmachineclusterpreference",
  "name": "cirros-cluster"
}
# ./cluster-up/virtctl.sh start demo-cluster &&  ./cluster-up/kubectl.sh wait vms/demo-cluster --for=condition=Ready
diff --color -u <( ./cluster-up/kubectl.sh get vms/demo-cluster -o json | jq .spec.template.spec) <( ./cluster-up/kubectl.sh get vmis/demo-cluster -o json | jq .spec)
selecting docker as container runtime
VM demo-cluster was scheduled to start
selecting docker as container runtime
virtualmachine.kubevirt.io/demo-cluster condition met
selecting docker as container runtime
selecting docker as container runtime
--- /dev/fd/63	2022-08-03 13:37:04.897273573 +0100
+++ /dev/fd/62	2022-08-03 13:37:04.897273573 +0100
@@ -1,15 +1,65 @@
 {
   "domain": {
-    "devices": {},
+    "cpu": {
+      "cores": 1,
+      "model": "host-model",
+      "sockets": 2,
+      "threads": 1
+    },
+    "devices": {
+      "disks": [
+        {
+          "disk": {
+            "bus": "virtio"
+          },
+          "name": "containerdisk"
+        },
+        {
+          "disk": {
+            "bus": "virtio"
+          },
+          "name": "cloudinitdisk"
+        }
+      ],
+      "interfaces": [
+        {
+          "bridge": {},
+          "model": "virtio",
+          "name": "default"
+        }
+      ]
+    },
+    "features": {
+      "acpi": {
+        "enabled": true
+      }
+    },
+    "firmware": {
+      "uuid": "05fa1ec0-3e45-581d-84e2-36ddc6b50633"
+    },
     "machine": {
       "type": "q35"
     },
-    "resources": {}
+    "memory": {
+      "guest": "128Mi"
+    },
+    "resources": {
+      "requests": {
+        "memory": "128Mi"
+      }
+    }
   },
+  "networks": [
+    {
+      "name": "default",
+      "pod": {}
+    }
+  ],
   "volumes": [
     {
       "containerDisk": {
-        "image": "registry:5000/kubevirt/cirros-container-disk-demo:devel"
+        "image": "registry:5000/kubevirt/cirros-container-disk-demo:devel",
+        "imagePullPolicy": "IfNotPresent"
       },
       "name": "containerdisk"
     },
# #
# Demo #3 Instancetypes vs Preferences vs VirtualMachine
# #
# Users cannot overwrite anything set by an instancetype in their VirtualMachine, for example CPU topologies
cat <<EOF | ./cluster-up/kubectl.sh apply -f -
---
apiVersion: kubevirt.io/v1
kind: VirtualMachine
metadata:
  name: demo-instancetype-conflict
spec:
  instancetype:
    kind: VirtualMachineInstancetype
    name: small
  preference:
    kind: VirtualMachinePreference
    name: cirros
  running: false
  template:
    spec:
      domain:
        cpu:
          threads: 1
          cores: 3
          sockets: 1
        devices: {}
      volumes:
      - containerDisk:
          image: registry:5000/kubevirt/cirros-container-disk-demo:devel
        name: containerdisk
      - cloudInitNoCloud:
          userData: |
            #!/bin/sh

            echo 'printed from cloud-init userdata'
        name: cloudinitdisk
EOF
selecting docker as container runtime
The request is invalid: spec.template.spec.domain.cpu: VM field conflicts with selected Instancetype
# #
# Users can however overwrite anything set by a preference in their VirtualMachine, for example disk buses etc.
cat <<EOF | ./cluster-up/kubectl.sh apply -f -
---
apiVersion: kubevirt.io/v1
kind: VirtualMachine
metadata:
  name: demo-instancetype-user-preference
spec:
  instancetype:
    kind: VirtualMachineInstancetype
    name: small
  preference:
    kind: VirtualMachinePreference
    name: cirros
  running: false
  template:
    spec:
      domain:
        devices:
          disks:
          - disk:
              bus: sata
            name: containerdisk
      volumes:
      - containerDisk:
          image: registry:5000/kubevirt/cirros-container-disk-demo:devel
        name: containerdisk
      - cloudInitNoCloud:
          userData: |
            #!/bin/sh

            echo 'printed from cloud-init userdata'
        name: cloudinitdisk
EOF
selecting docker as container runtime
virtualmachine.kubevirt.io/demo-instancetype-user-preference created
# ./cluster-up/virtctl.sh start demo-instancetype-user-preference && ./cluster-up/kubectl.sh wait vms/demo-instancetype-user-preference --for=condition=Ready
diff --color -u <( ./cluster-up/kubectl.sh get vms/demo-instancetype-user-preference -o json | jq .spec.template.spec) <( ./cluster-up/kubectl.sh get vmis/demo-instancetype-user-preference -o json | jq .spec)
selecting docker as container runtime
VM demo-instancetype-user-preference was scheduled to start
selecting docker as container runtime
virtualmachine.kubevirt.io/demo-instancetype-user-preference condition met
selecting docker as container runtime
selecting docker as container runtime
--- /dev/fd/63	2022-08-03 13:37:38.099537528 +0100
+++ /dev/fd/62	2022-08-03 13:37:38.099537528 +0100
@@ -1,5 +1,11 @@
 {
   "domain": {
+    "cpu": {
+      "cores": 1,
+      "model": "host-model",
+      "sockets": 2,
+      "threads": 1
+    },
     "devices": {
       "disks": [
         {
@@ -7,18 +13,53 @@
             "bus": "sata"
           },
           "name": "containerdisk"
+        },
+        {
+          "disk": {
+            "bus": "virtio"
+          },
+          "name": "cloudinitdisk"
+        }
+      ],
+      "interfaces": [
+        {
+          "bridge": {},
+          "model": "virtio",
+          "name": "default"
         }
       ]
     },
+    "features": {
+      "acpi": {
+        "enabled": true
+      }
+    },
+    "firmware": {
+      "uuid": "195ea4a3-8505-5368-b068-9536257886ea"
+    },
     "machine": {
       "type": "q35"
     },
-    "resources": {}
+    "memory": {
+      "guest": "128Mi"
+    },
+    "resources": {
+      "requests": {
+        "memory": "128Mi"
+      }
+    }
   },
+  "networks": [
+    {
+      "name": "default",
+      "pod": {}
+    }
+  ],
   "volumes": [
     {
       "containerDisk": {
-        "image": "registry:5000/kubevirt/cirros-container-disk-demo:devel"
+        "image": "registry:5000/kubevirt/cirros-container-disk-demo:devel",
+        "imagePullPolicy": "IfNotPresent"
       },
       "name": "containerdisk"
     },
# #
# Demo #4 Versioning
# #
# We have versioning of instancetypes and preferences, note that the InstancetypeMatcher and PreferenceMatcher now have a populated revisionName field
./cluster-up/kubectl.sh get vms/demo -o json | jq '.spec.instancetype, .spec.preference'
selecting docker as container runtime
{
  "kind": "VirtualMachineInstancetype",
  "name": "small",
  "revisionName": "demo-small-4a28a2f3-fd34-421a-98d8-a2659f9a8eb7-1"
}
{
  "kind": "VirtualMachinePreference",
  "name": "cirros",
  "revisionName": "demo-cirros-d08c3914-7d2b-43b4-a295-9cd3687bf151-1"
}
# #
# These are the names of ControllerRevisions containing a copy of the VirtualMachine{Instancetype,Preference}Spec at the time of application
./cluster-up/kubectl.sh get controllerrevisions/$( ./cluster-up/kubectl.sh get vms/demo -o json | jq .spec.instancetype.revisionName |  tr -d '"') -o json | jq .
./cluster-up/kubectl.sh get controllerrevisions/$( ./cluster-up/kubectl.sh get vms/demo -o json | jq .spec.preference.revisionName | tr -d '"') -o json | jq .
selecting docker as container runtime
selecting docker as container runtime
{
  "apiVersion": "apps/v1",
  "data": {
    "apiVersion": "",
    "spec": "eyJjcHUiOnsiZ3Vlc3QiOjJ9LCJtZW1vcnkiOnsiZ3Vlc3QiOiIxMjhNaSJ9fQ=="
  },
  "kind": "ControllerRevision",
  "metadata": {
    "creationTimestamp": "2022-08-03T12:36:20Z",
    "name": "demo-small-4a28a2f3-fd34-421a-98d8-a2659f9a8eb7-1",
    "namespace": "default",
    "ownerReferences": [
      {
        "apiVersion": "kubevirt.io/v1",
        "blockOwnerDeletion": true,
        "controller": true,
        "kind": "VirtualMachine",
        "name": "demo",
        "uid": "e67ad6ba-7792-40ab-9cd2-a411b6161971"
      }
    ],
    "resourceVersion": "53965",
    "uid": "3f20c656-ea33-45d1-9195-fb3c4f7085b9"
  },
  "revision": 0
}
selecting docker as container runtime
selecting docker as container runtime
{
  "apiVersion": "apps/v1",
  "data": {
    "apiVersion": "",
    "spec": "eyJkZXZpY2VzIjp7InByZWZlcnJlZERpc2tCdXMiOiJ2aXJ0aW8iLCJwcmVmZXJyZWRJbnRlcmZhY2VNb2RlbCI6InZpcnRpbyJ9fQ=="
  },
  "kind": "ControllerRevision",
  "metadata": {
    "creationTimestamp": "2022-08-03T12:36:20Z",
    "name": "demo-cirros-d08c3914-7d2b-43b4-a295-9cd3687bf151-1",
    "namespace": "default",
    "ownerReferences": [
      {
        "apiVersion": "kubevirt.io/v1",
        "blockOwnerDeletion": true,
        "controller": true,
        "kind": "VirtualMachine",
        "name": "demo",
        "uid": "e67ad6ba-7792-40ab-9cd2-a411b6161971"
      }
    ],
    "resourceVersion": "53966",
    "uid": "dc47f75f-b548-41fd-b0db-8af4b458994b"
  },
  "revision": 0
}
# # With versioning we can update the VirtualMachineInstancetype, create a new VirtualMachine to assert the changes and then check that our original VirtualMachine hasn't changed
cat <<EOF | ./cluster-up/kubectl.sh apply -f -
---
apiVersion: instancetype.kubevirt.io/v1alpha1
kind: VirtualMachineInstancetype
metadata:
  name: small
spec:
  cpu:
    guest: 3
  memory:
    guest: 256Mi
---
apiVersion: instancetype.kubevirt.io/v1alpha1
kind: VirtualMachinePreference
metadata:
  name: cirros
spec:
  cpu:
    preferredCPUTopology: preferCores
  devices:
    preferredDiskBus: virtio
    preferredInterfaceModel: virtio
---
apiVersion: kubevirt.io/v1
kind: VirtualMachine
metadata:
  name: demo-updated
spec:
  instancetype:
    kind: VirtualMachineInstancetype
    name: small
  preference:
    kind: VirtualMachinePreference
    name: cirros
  running: false
  template:
    spec:
      domain:
        devices: {}
      volumes:
      - containerDisk:
          image: registry:5000/kubevirt/cirros-container-disk-demo:devel
        name: containerdisk
      - cloudInitNoCloud:
          userData: |
            #!/bin/sh

            echo 'printed from cloud-init userdata'
        name: cloudinitdisk
EOF
selecting docker as container runtime
virtualmachineinstancetype.instancetype.kubevirt.io/small configured
virtualmachinepreference.instancetype.kubevirt.io/cirros configured
virtualmachine.kubevirt.io/demo-updated created
# #
# Now start the updated VirtualMachine
./cluster-up/virtctl.sh start demo-updated &&  ./cluster-up/kubectl.sh wait vms/demo-updated --for=condition=Ready
selecting docker as container runtime
VM demo-updated was scheduled to start
selecting docker as container runtime
virtualmachine.kubevirt.io/demo-updated condition met
# #
# We now see the updated instancetype used by the new VirtualMachine and applied to the VirtualMachineInstance
diff --color -u <( ./cluster-up/kubectl.sh get vms/demo-updated -o json | jq .spec.template.spec) <( ./cluster-up/kubectl.sh get vmis/demo-updated -o json | jq .spec)
selecting docker as container runtime
selecting docker as container runtime
--- /dev/fd/63	2022-08-03 13:38:37.203007409 +0100
+++ /dev/fd/62	2022-08-03 13:38:37.204007417 +0100
@@ -1,15 +1,65 @@
 {
   "domain": {
-    "devices": {},
+    "cpu": {
+      "cores": 3,
+      "model": "host-model",
+      "sockets": 1,
+      "threads": 1
+    },
+    "devices": {
+      "disks": [
+        {
+          "disk": {
+            "bus": "virtio"
+          },
+          "name": "containerdisk"
+        },
+        {
+          "disk": {
+            "bus": "virtio"
+          },
+          "name": "cloudinitdisk"
+        }
+      ],
+      "interfaces": [
+        {
+          "bridge": {},
+          "model": "virtio",
+          "name": "default"
+        }
+      ]
+    },
+    "features": {
+      "acpi": {
+        "enabled": true
+      }
+    },
+    "firmware": {
+      "uuid": "937dc645-17f0-599b-be81-c1e9dbde8075"
+    },
     "machine": {
       "type": "q35"
     },
-    "resources": {}
+    "memory": {
+      "guest": "256Mi"
+    },
+    "resources": {
+      "requests": {
+        "memory": "256Mi"
+      }
+    }
   },
+  "networks": [
+    {
+      "name": "default",
+      "pod": {}
+    }
+  ],
   "volumes": [
     {
       "containerDisk": {
-        "image": "registry:5000/kubevirt/cirros-container-disk-demo:devel"
+        "image": "registry:5000/kubevirt/cirros-container-disk-demo:devel",
+        "imagePullPolicy": "IfNotPresent"
       },
       "name": "containerdisk"
     },
# #
# With new ControllerRevisions referenced from the underlying VirtualMachine
./cluster-up/kubectl.sh get vms/demo-updated -o json | jq '.spec.instancetype, .spec.preference'
selecting docker as container runtime
{
  "kind": "VirtualMachineInstancetype",
  "name": "small",
  "revisionName": "demo-updated-small-4a28a2f3-fd34-421a-98d8-a2659f9a8eb7-2"
}
{
  "kind": "VirtualMachinePreference",
  "name": "cirros",
  "revisionName": "demo-updated-cirros-d08c3914-7d2b-43b4-a295-9cd3687bf151-2"
}
# #
# We can also stop and start the original VirtualMachine without changing the VirtualMachineInstance it spawns
./cluster-up/virtctl.sh stop demo &&  ./cluster-up/kubectl.sh wait vms/demo --for=condition=Ready=false
./cluster-up/virtctl.sh start demo &&  ./cluster-up/kubectl.sh wait vms/demo --for=condition=Ready
diff --color -u <( ./cluster-up/kubectl.sh get vms/demo -o json | jq .spec.template.spec) <( ./cluster-up/kubectl.sh get vmis/demo -o json | jq .spec)
selecting docker as container runtime
VM demo was scheduled to stop
selecting docker as container runtime
virtualmachine.kubevirt.io/demo condition met
selecting docker as container runtime
Error starting VirtualMachine Operation cannot be fulfilled on virtualmachine.kubevirt.io "demo": VM is already running
selecting docker as container runtime
selecting docker as container runtime
--- /dev/fd/63	2022-08-03 13:38:51.291119408 +0100
+++ /dev/fd/62	2022-08-03 13:38:51.291119408 +0100
@@ -1,15 +1,65 @@
 {
   "domain": {
-    "devices": {},
+    "cpu": {
+      "cores": 1,
+      "model": "host-model",
+      "sockets": 2,
+      "threads": 1
+    },
+    "devices": {
+      "disks": [
+        {
+          "disk": {
+            "bus": "virtio"
+          },
+          "name": "containerdisk"
+        },
+        {
+          "disk": {
+            "bus": "virtio"
+          },
+          "name": "cloudinitdisk"
+        }
+      ],
+      "interfaces": [
+        {
+          "bridge": {},
+          "model": "virtio",
+          "name": "default"
+        }
+      ]
+    },
+    "features": {
+      "acpi": {
+        "enabled": true
+      }
+    },
+    "firmware": {
+      "uuid": "c89d1344-ee03-5c55-99bd-5df16b72bea0"
+    },
     "machine": {
       "type": "q35"
     },
-    "resources": {}
+    "memory": {
+      "guest": "128Mi"
+    },
+    "resources": {
+      "requests": {
+        "memory": "128Mi"
+      }
+    }
   },
+  "networks": [
+    {
+      "name": "default",
+      "pod": {}
+    }
+  ],
   "volumes": [
     {
       "containerDisk": {
-        "image": "registry:5000/kubevirt/cirros-container-disk-demo:devel"
+        "image": "registry:5000/kubevirt/cirros-container-disk-demo:devel",
+        "imagePullPolicy": "IfNotPresent"
       },
       "name": "containerdisk"
     },
# #
# The ControllerRevisions are owned by the VirtualMachines, as such removal of the VirtualMachines now removes the ControllerRevisions
./cluster-up/kubectl.sh get controllerrevisions
./cluster-up/kubectl.sh delete vms/demo vms/demo-updated vms/demo-cluster vms/demo-instancetype-user-preference
./cluster-up/kubectl.sh get controllerrevisions
selecting docker as container runtime
NAME                                                                              CONTROLLER                                                     REVISION   AGE
demo-cirros-d08c3914-7d2b-43b4-a295-9cd3687bf151-1                                virtualmachine.kubevirt.io/demo                                0          2m51s
demo-cluster-cirros-cluster-1562ae69-8a4b-4a75-8507-e0da5041c5d2-1                virtualmachine.kubevirt.io/demo-cluster                        0          2m10s
demo-cluster-small-cluster-20c0a541-e24f-47c1-a1d7-1151e981a69c-1                 virtualmachine.kubevirt.io/demo-cluster                        0          2m10s
demo-instancetype-user-preference-cirros-d08c3914-7d2b-43b4-a295-9cd3687bf151-1   virtualmachine.kubevirt.io/demo-instancetype-user-preference   0          98s
demo-instancetype-user-preference-small-4a28a2f3-fd34-421a-98d8-a2659f9a8eb7-1    virtualmachine.kubevirt.io/demo-instancetype-user-preference   0          98s
demo-small-4a28a2f3-fd34-421a-98d8-a2659f9a8eb7-1                                 virtualmachine.kubevirt.io/demo                                0          2m51s
demo-updated-cirros-d08c3914-7d2b-43b4-a295-9cd3687bf151-2                        virtualmachine.kubevirt.io/demo-updated                        0          41s
demo-updated-small-4a28a2f3-fd34-421a-98d8-a2659f9a8eb7-2                         virtualmachine.kubevirt.io/demo-updated                        0          41s
local-volume-provisioner-55dcc65dc7                                               daemonset.apps/local-volume-provisioner                        1          3h32m
revision-start-vm-5786044a-c20b-41a4-bba3-c7744c624935-2                          virtualmachine.kubevirt.io/demo-instancetype-user-preference   2          98s
revision-start-vm-a334ac37-aed4-4b98-b8b9-af819f54ffda-2                          virtualmachine.kubevirt.io/demo-cluster                        2          2m10s
revision-start-vm-e67ad6ba-7792-40ab-9cd2-a411b6161971-2                          virtualmachine.kubevirt.io/demo                                2          2m51s
revision-start-vm-f19cf23d-0ad6-438b-a166-20879a704fa9-2                          virtualmachine.kubevirt.io/demo-updated                        2          41s
selecting docker as container runtime
virtualmachine.kubevirt.io "demo" deleted
virtualmachine.kubevirt.io "demo-updated" deleted
virtualmachine.kubevirt.io "demo-cluster" deleted
virtualmachine.kubevirt.io "demo-instancetype-user-preference" deleted
selecting docker as container runtime
NAME                                  CONTROLLER                                REVISION   AGE
local-volume-provisioner-55dcc65dc7   daemonset.apps/local-volume-provisioner   1          3h32m

```