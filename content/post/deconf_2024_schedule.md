---
title: "Devconf.cz 2024 - Schedule and talk"
date: 2024-05-02T11:00:00Z
tags: ['dev', 'kubevirt', 'devconf', 'presentation']
---

Happy and slightly nervous to highlight that a talk submitted on my behalf by a wonderful colleague while I was out on sick leave was accepted for devconf.cz this year and I'll be presenting in room D105 at 14:00 CEST on Thursday June 13th:

https://pretalx.com/devconf-cz-2024/talk/WPWMY7/

{{< figure src="/img/devconf_schedule.png" >}}

Very much looking forward to traveling and to see folks once again after almost 5 years since my last trip to Brno.