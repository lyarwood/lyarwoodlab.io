---
title: "KubeVirt Summit 2023"
date: 2023-04-03T12:00:00Z
tags: 
  - dev
  - kubevirt
  - kubevirtsummit
  - instancetypes
  - preferences
  - presentation
---

![](https://media.giphy.com/media/94iS62lx8CRQA/giphy.gif)

I had the pleasure of delivering a very short KubeVirt Summit 2023 presentation with my colleague Felix last week covering some of our work around instance types and `virtctl` over the last year. Please find the slides and a recording below.

## Video

{{< youtube qncSQmO1G4M >}}

## Slides

<iframe width="100%" height="400" name="iframe" src="/presentations/kubevirt/summit/2023/presentation.html">
</iframe>