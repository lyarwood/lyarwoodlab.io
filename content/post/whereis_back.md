---
title: "whereis lyarwood? - Back online!"
date: 2024-02-20T08:00:00Z
tags: 
  - personal
  - whereis
---

{{< figure src="/img/back.gif" >}}

After just over 4 months off I'm returning to work on Monday February 26th.

What follows is a brief overview of why I was offline in an attempt to avoid repeating myself and likely boring people with the story over the coming weeks.

**tl;dr - If in doubt always seek multiple medical opinions!**

As crudely documented at the time on my private [instagram account](https://www.instagram.com/lyarwood_/) and later copied into this blog I was admitted to hospital back in October after finally getting a second opinion on some symptoms I had been having for the prior ~6 months. These symptoms included fevers, uncontrollable shivering and exhaustion but had unfortunately been misdiagnosed as night sweats. I was about to see an Endocrinologist when my condition really deteriorated and my wife finally pushed me to seek the advice of a second GP.

Within a few minutes of seeing the GP a new diagnosis was suggested and I was quickly admitted to Hospital. There we discovered that my [ICD](https://en.wikipedia.org/wiki/Implantable_cardioverter-defibrillator), first implanted 19 years earlier for primary prevention, had become completely infected with a ~40mm long mass (yum!) hanging onto the end of the wire within my heart. The eventual diagnosis would be cardiac device related infective endocarditis. I was extremely lucky that the mass hadn't detached already causing a fatal heart attack or stroke.

If that wasn't enough I also somehow managed to contract COVID within a few days of being in hospital and had to spend a considerable amount of time in isolation while my initial course of antibiotics were being given. This was definitely a low point but the Coronary Care Unit and Lugg Ward teams at Hereford County Hospital were excellent throughout.

{{< figure src="/img/barts.png" >}}

Once I was COVID negative I was transferred to St Bartholomew's Hospital in London and had an emergency device extraction via open heart surgery on November 14th. While the surgery went well there were complications in the form of liquid (~600ml) building up around my heart and lungs that led to more surgery at the start of December. During this time I remained on a cocktail of IV antibiotics that finally stopped in the middle of December and after numerous blood cultures (including one false positive!) I had an [S-ICD](https://en.wikipedia.org/wiki/Subcutaneous_implantable_defibrillator) implanted on December 20th before being discharged home the next day.

{{< figure src="/img/mon.png" >}}

I've spent the time since recovering at home with family as my sternum and numerous wounds slowly heal. This time has been extremely important and I can't thank Red Hat and my management chain enough for their support with this extended sick leave. I've been able to focus on reconnecting with my wife and girls Rose (3.5) and Phoebe (9 months) after 9 extremely difficult weeks apart. I've also ventured back to London for checkups and to thank friends who helped me through the weeks away from home. I've got many many more people to thank in the coming months.

I now feel mentally ready to return to work but know it's going to be a little while longer before I'm fully physically recovered.

Thanks to anyone who reached out during this time and I'll catch you all upstream in the coming weeks!

### **~Update 26/02/24~**

I'm back to work today, many thanks once again for all of the responses to this on LinkedIn and elsewhere, they are all greatly appreciated!

{{< figure src="/img/fam.png" >}}