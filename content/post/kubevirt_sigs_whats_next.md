---
title: "KubeVirt SIGs - What's Next?"
date: 2023-08-25T08:00:00Z
tags: 
  - dev
  - kubevirt
  - sigs
---

{{< figure src="/img/KubeVirt_logo.png" >}}

A quick post to highlight that I'm currently leading a [weekly call](https://groups.google.com/g/kubevirt-dev/c/uj_YXOS2aXs) aiming to continue the work of defining and then seeding special interest groups or SIGs within the KubeVirt community.

At present the call is focused on the [following topics](https://docs.google.com/document/d/1yOtxWS2eIgA2VT71NbFaDEzGyVfmiDCwoRBHKow0K6c/edit?usp=sharing) and an associated [WIP `kubevirt/community` pull request](https://github.com/kubevirt/community/pull/236) implementing the required changes:

- Defining the high level aspects of SIGs within KubeVirt using k8s as a starting point
  - What is a SIG?
  - What should a SIG do?
  - How do we create and remove SIGs?
- Populating `kubevirt/community` with an initial set of SIGs
- Populating a single SIG and creating the required collateral (such as a charter etc) within `kubevirt/community`
- Assigning code ownership using OWNERS files to the SIG across the core `kubevirt/kubevirt` project

There are also topics outside of this such as the need for a steering committee like group to oversee the process in the future that are also being discussed but these might be eventually handled outside of the call.

All current and potential future contributors to the KubeVirt project are welcome on the call so please do feel free to join if you would like to help shape this future aspect of the project!

Please note however that the calls have now moved to a new time slot of Thursday @ 11:00 EDT / 14:00 UTC / 15:00 BST / 16:00 CEST after a number of last minute cancellations due to downstream conflicts.

Finally, please find the recording of our previous call from August 17th is available below to review our previous discussions:

{{< youtube u4lDvaq6Dho >}}