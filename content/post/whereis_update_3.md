---
title: "whereis lyarwood? - Battered, bruised but not defeated - Update #3"
date: 2023-11-18T08:00:00Z
tags: 
  - personal
  - whereis
---

Surgery on Tuesday went well but they had to leave a small bit of plastic sheath that had grown into the wall of a vein. Unknown what that means in the medium and long term but for now the ICD and bulk of the infection has gone.

I've been up on my feet and while everything is sore and painful I am slowly getting better each day. I've even had a MRI scan today to help decide if I need an S-ICD in the future.

Still lots of tests and antibiotics to take before I can even think about home but I'm getting closer to my girls with every passing minute.

Thanks again for the messages, apologies if I've not replied but the last couple of days have quite literally been hell.