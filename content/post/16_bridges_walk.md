+++
title = "16 bridges charity walk"
Description = ""
date = "2017-07-23T16:11:58+05:30"
Tags = ["charity", "walking"]
aliases = [
    "/2017/07/23/walking/"
]
+++

As I alluded to in my opening post Katie and I are raising money for
[Cardiomyopathy UK](http://www.cardiomyopathy.org/) over the coming months,
starting with a walk through London in September. But why, you might ask, are
we doing this? 

{{< figure src="/img/cardio.jpg" >}}

Well, the charity held a conference that we both attended shortly after I was
hospitalised in August 2016. Thankfully that episode has since been
deemed to simply be [Atrial
fibrillation](https://en.wikipedia.org/wiki/Atrial_fibrillation) that my
[ICD](https://en.wikipedia.org/wiki/Implantable_cardioverter-defibrillator)
mistook for [Ventricular
fibrillation](https://en.wikipedia.org/wiki/Ventricular_fibrillation), a very
serious and potentially life threatening issue. 

At the time we were both struggling to deal with the reality of my long
suspected, but never fully confirmed condition
[ARVC](https://en.wikipedia.org/wiki/Arrhythmogenic_right_ventricular_dysplasia) presenting itself.
The Cardiomyopathy UK National Conference in November of last year was
brilliant, eye opening and helped greatly during that time. Now that things
aren't so bleak we wanted to give something back and this walk is the first
step (ha!) in achieving that. 

We've just been given the 25km route that I've included below :

<iframe src="https://www.google.com/maps/d/embed?mid=1UJ_WvEdg3Y46qC5Nna6qipTPc5c" width="640" height="480"></iframe>

We've also started training with a few slow jaunts around Hereford : 

<iframe src='https://connect.garmin.com/modern/activity/embed/1869326372' title='Holmer & Shelwick' width='465' height='500' frameborder='0'></iframe>

<iframe src='https://connect.garmin.com/modern/activity/embed/1871500214' title='Holmer & Shelwick' width='465' height='500' frameborder='0'></iframe>

<iframe src='https://connect.garmin.com/modern/activity/embed/1882996233' title='Holmer & Shelwick' width='465' height='500' frameborder='0'></iframe>

<iframe src='https://connect.garmin.com/modern/activity/embed/1899763663' title='Training walk #4' width='465' height='500' frameborder='0'></iframe>

<iframe src='https://connect.garmin.com/modern/activity/embed/1949980268' title='Training walk #5' width='465' height='500' frameborder='0'></iframe>

Finally, if you would like to donate, it would be very much appreciated. Your money will help people like us, going through scary and confusing times, to access information, support and advice as well as contribute to training for medical professionals. We are gratefully accepting donations via our [justgiving.com page](https://www.justgiving.com/fundraising/yarwoods) with our current progress towards our goal shown below  :

<iframe src='/donations.html' width='640' height='300' frameborder='0'></iframe>
