---
title: "KubeVirt instancetype.kubevirt.io Demo #3"
date: 2022-10-27T09:00:00Z
asciinema: true
tags: 
  - dev
  - kubevirt
  - instancetype
  - instancetypes
  - flavors
  - preferences
  - demo
---

{{< figure src="/img/KubeVirt_logo.png" >}}

This is my [third demo for KubeVirt](/tags/demo), this time introducing the following features and bugfixes:

* A new `v1alpha2` instancetype API version
* New `AutoAttachInputDevice` auto attach & `PreferredAutoAttachInputDevice` preference attributes
* A bugfix for the `PreferredMachineType` preference

If you've been following my [instance type development blog series](/tags/instancetype) you will note that some of these aren't that recent but as I've not covered them in a demo until now I wanted to touch on them again.

Expect more demos in the coming weeks as I catch up with the current state of development.

## Recording

{{< asciinema key="kubevirt/instancetypes/3-instancetypes-v1alpha2/demo.cast" preload="0" fit="width">}}

## Transcript

I'm not going to include a complete transcript this time but the [script and associated examples are available on my demos repo.](https://github.com/lyarwood/demos/tree/main/kubevirt/instancetypes/3-instancetypes-v1alpha2)