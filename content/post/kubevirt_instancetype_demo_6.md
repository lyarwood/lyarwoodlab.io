---
title: "KubeVirt instancetype.kubevirt.io Demo #6"
date: 2023-07-05T09:00:00Z
asciinema: true
tags: 
  - dev
  - kubevirt
  - instancetype
  - instancetypes
  - flavors
  - preferences
  - demo
---

{{< figure src="/img/KubeVirt_logo.png" >}}

This is my [sixth demo post for KubeVirt](/tags/demo), this time introducing the following features and bugfixes:

* A new `v1beta1` `instancetype.kubevirt.io` version and associated features
* A new `common-instancetypes` `v0.3.0` version and associated features

## Recording

{{< asciinema key="kubevirt/instancetypes/6-v1beta1/demo.cast" preload="0" fit="width">}}

## Transcript

The [script and associated examples are available on my demos repo.](https://github.com/lyarwood/demos/tree/main/kubevirt/instancetypes/6-v1beta1)