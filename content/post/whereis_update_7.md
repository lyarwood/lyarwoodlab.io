---
title: "whereis lyarwood? - The 80 20 rule - Update #7"
date: 2023-12-14T08:00:00Z
tags: 
  - personal
  - whereis
---

Unfortunately while I'm feeling much better and basically back to normal a set of blood cultures taken last Friday somehow managed to grow bacteria from the same family (Staphylococcaceae) as my original infection. Additional cultures taken on Saturday and Monday did not growing anything leading to the assumption that the first set were somehow contaminated.

To ensure this really was the case another two sets of cultures are being taken today and should allow for the all clear to be given some time on Sunday evening.

This has obviously delayed my final S-ICD implant surgery but I've been assured that once the all clear is given the surgery should be able to take place on Monday or Tuesday at the latest. If anything does grow then I'm in for another 10 to 14 days of antibiotics before we try the process again.

I'm pretty gutted after assuming I'd be coming home this week but hopeful that things work out in the coming days and I can get home early next week instead.

Thanks as always to folks sending messages, visiting or helping out at home. We are almost there now.