---
title: "whereis lyarwood? - Progress - Update #6"
date: 2023-12-08T08:00:00Z
tags: 
  - personal
  - whereis
---

The last 7 days have been extremely difficult. From being told by a nurse that my pain tolerance is too low (it isn't, see below about my lung), having a drain and bucket of blood attached to me for 4 days and being left all weekend without my phone, laptop, tablet and glasses it has honestly been the most trying week of my life.

The surgery to remove liquid from around my heart was successful (~750ml removed over 4 days) but we then discovered yet more liquid around my partially collapsed left lung. Thankfully the latter just required rest, exercise and time to clear and I was given the all clear yesterday for both.

This then allowed my antibiotics to finally be stopped as my infection/inflammation blood markers all came crashing down. It honestly felt weird not being hooked up to an IV for 3 hours while I attempted to sleep last night.

With the antibiotics stopped I could then be booked in for S-ICD implant surgery on Monday, the final thing required before I can go home.

All being well this means that I should be discharged sometime on Tuesday and finally able to go home to see my girls for the first time in almost 8 weeks.

To celebrate I've just ventured outside on my own and had a flat white for the first time since I was admitted.

As always I can't thank everyone who made the effort to message, visit or help out back home enough. I can't imagine what this would have been like without your support so thank you from the bottom of my now disinfected and hopefully healthy heart!