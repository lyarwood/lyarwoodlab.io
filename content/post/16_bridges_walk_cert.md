+++
title = "16 bridges charity walk - certificate"
date = "2017-10-25T16:11:58+05:30"
Tags = ["charity", "walking"]
+++

I know, I know, another post about our 16 bridges walk. We just received our
certificate from Cardiomyopathy UK showing that we raised a grand total of
£635.00. My thanks again to everyone who donated!
