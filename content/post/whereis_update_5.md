---
title: "whereis lyarwood? - Bump in the road - Update #5"
date: 2023-12-01T08:00:00Z
tags: 
  - personal
  - whereis
---

Morning all, unfortunately I'm heading back down to the basement at St Bart's for more surgery today after liquid has built up around my heart.

This is a common complication from cardiac surgery and the procedure should in all likelihood be simple and over in about 60 minutes.

With this hopefully resolved I should then see my recovery continue after it platod over the last week or so, even with blood transfusions and the best efforts of the staff on the recovery ward.

Then it's back to ensuring all of my inflammation and infection markers return to normal before stopping antibiotics and collecting blood cultures to ensure the infection is no more.

Once that's done I'm then having an S-ICD implanted as an inpatient before leaving the hospital. This sits under the skin and doesn't have a wire in the heart.

For anyone still following along it increasingly looks like I'm not going to be home for Christmas but given the recent discovery I'd rather return home fully fit than have to return here again in the new year.

Thank you once again to everyone who is chipping in at home with the girls, visiting me in London or just messaging (apologies if I don't reply, often my head is just mush at the moment). We massively appreciate the support!