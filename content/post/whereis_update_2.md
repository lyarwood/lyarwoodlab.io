---
title: "whereis lyarwood? - All hail the PICC line - Update #2"
date: 2023-11-11T08:00:00Z
tags: 
  - personal
  - whereis
---

Another short update from me, surgery has been confirmed and booked for Tuesday morning. Katie is traveling up Monday and then it's game on. Unfortunately the beard and most of the hair on my upper body has to go but it will grow back eventually.

Goes without saying but thank you to everyone helping out at home, the distance is starting to hurt but with surgery I'll be one step closer to home next week.

Hopefully out of ICU by the end of the week and posting more boring updates. In the meantime keep the girls in your thoughts and I'll catch you all on the other side.