---
title: "KubeVirt instancetype.kubevirt.io Updated `InferFromVolume` demo"
date: 2023-01-19T09:00:00Z
asciinema: true
tags: 
  - dev
  - kubevirt
  - instancetype
  - instancetypes
  - flavors
  - preferences
  - demo
---

{{< figure src="/img/KubeVirt_logo.png" >}}

The implementation of this feature has now landed and will be included in the upcoming `v0.59.0` release of KubeVirt.

https://github.com/kubevirt/kubevirt/pull/8480

## Updated Design

There have been some small changes to the design. Notably that `DataSources` are now supported as a target of `InferFromVolume` and 
the previously listed camel-case annotations are now hyphenated labels:

* `instancetype.kubevirt.io/default-instancetype`
* `instancetype.kubevirt.io/default-instancetype-kind` (Defaults to `VirtualMachineClusterInstancetype`)
* `instancetype.kubevirt.io/default-preference`
* `instancetype.kubevirt.io/default-preference-kind` (Defaults to `VirtualMachineClusterPreference`)

## Demo

I've recorded a new demo below using a SSP operator development environment, the demo now covers the following:

* The creation of decorated `DataImportCrons` by the SSP operator
* The deployment of `kubevirt/common-instancetypes` by the SSP operator
* The creation of decorated `DataSources` and `PVCs` by CDI
* The ability for KubeVirt to infer the default instance type and preference from a `DataSource` for a `VirtualMachine`

https://github.com/lyarwood/demos/tree/main/kubevirt/instancetypes/5-infer-default-preferences-with-common-instancetypes

{{< asciinema key="kubevirt/instancetypes/5-infer-default-preferences-with-common-instancetypes/demo.cast" preload="0" fit="width">}}
