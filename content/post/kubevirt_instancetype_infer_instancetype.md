---
title: "KubeVirt instancetype.kubevirt.io `InferFromVolume` demo"
date: 2022-10-31T09:00:00Z
asciinema: true
tags: 
  - dev
  - kubevirt
  - instancetype
  - instancetypes
  - flavors
  - preferences
  - demo
---

{{< figure src="/img/KubeVirt_logo.png" >}}

As I alluded to in my [previous demo post]({{< ref "/post/kubevirt_instancetype_demo_3.md" >}}) we have some exciting new features currently under development concerning instance types and preferences. This post introduces one of these, the ability for KubeVirt to infer the default instance type and preference of a `VirtualMachine` from a suggested `Volume`.

## Current Design

The current design document PR is listed below:

design-proposals: Default instance types inferred from volumes
https://github.com/kubevirt/community/pull/190

The tl;dr being that the `{Instancetype,Preference}Matchers` will be extended with an `inferFromVolume` attribute that will reference the name of a `Volume` associatied with the `VirtualMachine`. This `Volume` will then be used to infer defaults by looking for the following annotations:

* `instancetype.kubevirt.io/defaultInstancetype`
* `instancetype.kubevirt.io/defaultInstancetypeKind` (Defaults to `VirtualMachineClusterInstancetype`)
* `instancetype.kubevirt.io/defaultPreference`
* `instancetype.kubevirt.io/defaultPreferenceKind` (Defaults to `VirtualMachineClusterPreference`)

Initially only `PVC` and `DataVolume` derived `Volumes` will be supported but this will likely be extended to anything with annotations, such as `Containers`.

Feedback is welcome in the design review document if you have any!

## Demo

This demo is based on some work in progress code posted below:

WIP - Introduce support for default instance type and preference PVC annotations
https://github.com/kubevirt/kubevirt/pull/8480

The demo script, resource definitions and asciinema recording can be found below:

https://github.com/lyarwood/demos/tree/main/kubevirt/instancetypes/4-infer-default-instancetypes 

{{< asciinema key="kubevirt/instancetypes/4-infer-default-instancetypes/demo.cast" preload="0" fit="width">}}
