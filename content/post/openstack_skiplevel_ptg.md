+++
title = "OpenStack - Skip level upgrades - PTG"
date = "2017-09-08T16:11:58+05:30"
tags = ["openstack", "tripleo", "skiplevelupgrades", "ptg"]
+++

{{< figure src="/img/ptg.png" >}}

# PTG

A short reminder that I'll be chairing the skip-level upgrades room at next
week's [OpenStack PTG in Denver](https://www.openstack.org/ptg/). So far ~15 of
you have shown interest in this track on the
[etherpad](https:////etherpad.openstack.org/p/queens-PTG-skip-level-upgrades)
so I'm looking forward to some useful discussions over the two days. For now we
still have available slots so if you do have suggestions please feel free to
add them directly on the pad!

At present the
[agenda](https:////etherpad.openstack.org/p/queens-PTG-skip-level-upgrades) for
the [room (Durango, Atrium
level)](https://www.openstack.org/assets/ptg/PTG-Denver-Schedule.pdf) looks
like this:


**Monday**

- 09:00 - 10:00 - ##### 
- 10:00 - 10:30 - Retrospective of what was discussed in Boston, outcomes, etc.
- 10:30 - 11:00 - Have operator requirements changed since Boston?
- 11:00 - 14:00 - #####
- 14:00 - 16:00 - What efforts (if any) are underway to enable skip level upgrades within the community?
- 16:00 - 18:00 - #####

**Tuesday**

- 09:00 - 10:30 - #####
- 10:30 - 11:00 - NFV considerations
- 11:00 - 11:30 - API versions control
- 11:30 - 14:00 - #####
- 14:00 - 16:00 - How can we collaborate and share tools for skip level upgrades within the community?
- 16:00 - 18:00 - Should we think about a different way of releasing?

# TripleO

Later in the week I will also be participating in the TripleO track, with a
[session on Thursday](https://etherpad.openstack.org/p/tripleo-ptg-queens) to
discuss my `WIP` [skip-level upgrade
spec](https://review.openstack.org/#/c/497257/). I'll be working on this during
the week leading up to this session so feel free to review this ahead of time
or just grab me in the hallway for a chat if this is something that interests
you! 
